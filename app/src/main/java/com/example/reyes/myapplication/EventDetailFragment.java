package com.example.reyes.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Set;

/**
 * A fragment representing a single Event detail screen.
 * This fragment is either contained in a {@link EventListActivity}
 * in two-pane mode (on tablets) or a {@link EventDetailActivity}
 * on handsets.
 */
public class EventDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item_id";
    private PointOfInterest mItem;
    private Button ignoreBtn;
    private Button completeBtn;
    private SharedPreferences prefs;
    Set<String> ignored = new HashSet<>();
    Set<String> completed = new HashSet<>();

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public EventDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            for (PointOfInterest poi : MapsActivity.pois) {
                if (poi.getID() == Integer.valueOf(getArguments().get(ARG_ITEM_ID).toString())) {
                    mItem = poi;
                }
            }
            for (PointOfInterest poi : MapsActivity.ignoredPois) {
                if (poi.getID() == Integer.valueOf(getArguments().get(ARG_ITEM_ID).toString())) {
                    mItem = poi;
                }
            }
            for (PointOfInterest poi : MapsActivity.completedPois) {
                if (poi.getID() == Integer.valueOf(getArguments().get(ARG_ITEM_ID).toString())) {
                    mItem = poi;
                }
            }
            Log.v("EventDetail", getArguments().get(ARG_ITEM_ID).toString());
            Log.v("EventDetail", mItem.toString());


            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.getName());
            }

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.event_detail, container, false);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            for (PointOfInterest poi : MapsActivity.pois) {
                if (poi.getID() == Integer.valueOf(getArguments().get(ARG_ITEM_ID).toString())) {
                    mItem = poi;
                }
            }
            for (PointOfInterest poi : MapsActivity.ignoredPois) {
                if (poi.getID() == Integer.valueOf(getArguments().get(ARG_ITEM_ID).toString())) {
                    mItem = poi;
                }
            }
            for (PointOfInterest poi : MapsActivity.completedPois) {
                if (poi.getID() == Integer.valueOf(getArguments().get(ARG_ITEM_ID).toString())) {
                    mItem = poi;
                }
            }
            Log.v("EventDetail", getArguments().get(ARG_ITEM_ID).toString());
            Log.v("EventDetail", mItem.toString());


            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.getName());
            }

        }

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.event_name)).setText(mItem.getName());
            ((TextView) rootView.findViewById(R.id.event_latlng)).setText(mItem.getPoint().toString());
            ((TextView) rootView.findViewById(R.id.event_expiry_date)).setText(mItem.getEndDate().toString());
        }

        ignoreBtn = rootView.findViewById(R.id.ignore);
        ignoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ignoreButtonClicked(v);
            }
        });
        completeBtn = rootView.findViewById(R.id.complete);
        completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                completeButtonClicked(v);
            }
        });

        prefs = rootView.getContext().getSharedPreferences("com.example.reyes.myapplication.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE);
        ignored = new HashSet<>(prefs.getStringSet("ignored", new HashSet<String>()));
        completed = new HashSet<>(prefs.getStringSet("completed", new HashSet<String>()));
        updateButtons();

        return rootView;
    }

    private void updateButtons() {
        if (ignored.contains(Integer.toString(mItem.getID()))) {
            ignoreBtn.setText("Don't ignore");
        } else {
            ignoreBtn.setText("Ignore");
        }

        if (completed.contains(Integer.toString(mItem.getID()))) {
            completeBtn.setText("Not done...");
        } else {
            completeBtn.setText("Done!");
        }
    }

    public void ignoreButtonClicked(View view) {
        Log.v("IgnoreButton", "Toggle ignore site " + mItem.toString());

        ignored = new HashSet<>(prefs.getStringSet("ignored", new HashSet<String>()));


        if (ignored.contains(Integer.toString(mItem.getID()))) {
            ignored.remove(Integer.toString(mItem.getID()));
        } else {
            ignored.add(Integer.toString(mItem.getID()));
        }
        updateButtons();

        SharedPreferences.Editor editor = prefs.edit();
        editor.putStringSet("ignored", ignored);
        editor.commit();

        prefs = view.getContext().getSharedPreferences("com.example.reyes.myapplication.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE);
        Log.v("IgnoreButton", prefs.getStringSet("ignore", new HashSet<String>()).toString());
    }

    public void completeButtonClicked(View view) {
        Log.v("CompleteButton", "Toggle complete site " + mItem.toString());

        completed = new HashSet<>(prefs.getStringSet("completed", new HashSet<String>()));


        if (completed.contains(Integer.toString(mItem.getID()))) {
            completed.remove(Integer.toString(mItem.getID()));
        } else {
            completed.add(Integer.toString(mItem.getID()));
        }
        updateButtons();

        SharedPreferences.Editor editor = prefs.edit();
        editor.putStringSet("completed", completed);
        editor.commit();

        prefs = view.getContext().getSharedPreferences("com.example.reyes.myapplication.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE);
        Log.v("CompleteButton", prefs.getStringSet("complete", new HashSet<String>()).toString());
    }
}
