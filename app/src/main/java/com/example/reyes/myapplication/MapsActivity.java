package com.example.reyes.myapplication;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {
    public static final int MY_PERMISSION_ACCESS_COARSE_LOCATION = 1;
    public static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 1;
    private static final String DATABASE_URL = "https://frozen-ravine-61657.herokuapp.com/events.json";

    private GoogleMap mMap;
    private GeofencingClient mGeofencingClient;
    private PendingIntent mGeofencePendingIntent;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;

    private List<Geofence> mGeofenceList = new LinkedList<>();
    public static List<PointOfInterest> pois = new LinkedList<>();
    public static List<PointOfInterest> ignoredPois = new LinkedList<>();
    public static List<PointOfInterest> completedPois = new LinkedList<>();
    private Location currentPos = null;
    private Marker userLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retrieve the content view that renders the map.
        setContentView(R.layout.activity_maps);

        if (pois.isEmpty() && ignoredPois.isEmpty() && completedPois.isEmpty())
            downloadPointsOfInterest();

        // Build the map.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGeofencingClient = LocationServices.getGeofencingClient(this);
        mFusedLocationClient = new FusedLocationProviderClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    centerOnUserLocation();
                }
            }
        };

        startLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        centerOnUserLocation();
        showPointsOfInterest();
        setFences();
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MapsActivity.MY_PERMISSION_ACCESS_FINE_LOCATION);
        }

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback,
                null);
    }

    private void downloadPointsOfInterest() {
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

            try {
                String response = HTTPGetCall(DATABASE_URL);
                JSONArray obj = new JSONArray(response);

                SharedPreferences prefs = getSharedPreferences("com.example.reyes.myapplication.PREFERENCE_FILE_KEY", Context.MODE_PRIVATE);
                Set<String> ignored = prefs.getStringSet("ignored", new HashSet<String>());
                Set<String> completed = prefs.getStringSet("completed", new HashSet<String>());

                Log.v("SharedPreferences", ignored.toString());
                Log.v("SharedPreferences", completed.toString());

                for (int i = 0; i < obj.length(); i++) {
                    JSONObject event = obj.getJSONObject(i);
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    Date date = null;
                    try {
                        date = format.parse(event.getString("end_date"));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    // Skip expired events
                    if (date.compareTo(Calendar.getInstance().getTime()) < 0)
                        continue;

                    PointOfInterest poi = new PointOfInterest(new LatLng(event.getDouble("lat"), event.getDouble("lng")),
                            (float) event.getDouble("radius"),
                            date,
                            event.getString("name"),
                            event.getInt("id"));

                    if (completed.contains(Integer.toString(poi.getID())))
                        completedPois.add(poi);
                    else if (ignored.contains(Integer.toString(poi.getID())))
                        ignoredPois.add(poi);
                    else
                        pois.add(poi);
                }

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        } else
            Log.v("Online", "Strict mode forced");

    }

    protected String HTTPGetCall(String WebMethodURL) throws IOException, MalformedURLException {
        StringBuilder response = new StringBuilder();

        //Prepare the URL and the connection
        URL u = new URL(WebMethodURL);
        HttpURLConnection conn = (HttpURLConnection) u.openConnection();

        if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
            //Get the Stream reader ready
            try (BufferedReader input = new BufferedReader(new InputStreamReader(conn.getInputStream()), 8192)) {

                String line = null;

                while ((line = input.readLine()) != null) {
                    response.append(line);
                }

                input.close();
            }
        }

        return response.toString();
    }

    private void centerOnUserLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    MapsActivity.MY_PERMISSION_ACCESS_COARSE_LOCATION);
        }
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MapsActivity.MY_PERMISSION_ACCESS_FINE_LOCATION);
        }

        Location location = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        if (location != null) {
            currentPos = location;

            LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());

            mMap.animateCamera(CameraUpdateFactory.newLatLng(latlng));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latlng)      // Sets the center of the map to location user
                    .zoom(15)                   // Sets the zoom
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            if (userLocation == null) {
                userLocation = mMap.addMarker(new MarkerOptions()
                        .position(latlng)
                        .title("You are here")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                userLocation.setTag(null);
            } else
                userLocation.setPosition(latlng);

        } else
            Log.v("Maps", "location is null");
    }

    private void showPointsOfInterest() {
        for (PointOfInterest poi : pois) {
            Marker m = mMap.addMarker(new MarkerOptions()
                    .position(poi.getPoint())
                    .title(poi.getName()));
            mMap.addCircle(new CircleOptions()
                    .center(poi.getPoint())
                    .radius(poi.getRadius())
                    .strokeColor(Color.RED)
                    .strokeWidth(1)
                    .fillColor(0x22FF0000));
            m.setTag(poi);
        }

        for (PointOfInterest poi : ignoredPois) {
            Marker m = mMap.addMarker(new MarkerOptions()
                    .position(poi.getPoint())
                    .title(poi.getName())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
            m.setTag(poi);
        }

        for (PointOfInterest poi : completedPois) {
            Marker m = mMap.addMarker(new MarkerOptions()
                    .position(poi.getPoint())
                    .title(poi.getName())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            m.setTag(poi);
        }

        // Set up "Click on text" marker
        mMap.setOnInfoWindowClickListener(this);
    }

    private void setFences() {
        for (PointOfInterest poi : pois) {
            mGeofenceList.add(new Geofence.Builder()
                    .setRequestId(String.valueOf(poi.getID()))
                    .setCircularRegion(
                            poi.getPoint().latitude,
                            poi.getPoint().longitude,
                            poi.getRadius())
                    .setExpirationDuration(poi.getEndDate().getTime() - Calendar.getInstance().getTimeInMillis())
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                    .build());
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MapsActivity.MY_PERMISSION_ACCESS_FINE_LOCATION);
        }
        if (!mGeofenceList.isEmpty())
            mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.v("Fencing", "" + mGeofenceList.size() + " fences added");
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.v("Fencing", "Couldn't set geofences");
                        }
                    });
        else
            Log.v("Fencing", "No geofences to add");
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mGeofenceList);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        // If this marker is user location, ignore
        if (marker.getTag() == null)
            return;
        PointOfInterest poi = (PointOfInterest) marker.getTag();

        Intent i = new Intent(this, EventDetailActivity.class);
        i.putExtra(EventDetailFragment.ARG_ITEM_ID, poi.getID());
        startActivity(i);
    }
}
