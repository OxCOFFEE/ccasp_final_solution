package com.example.reyes.myapplication;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;
import java.util.Objects;
import java.util.Random;

public class PointOfInterest {
    private LatLng point;
    private float radius;
    private Date endDate;
    private String name;
    private int ID;

    public LatLng getPoint() {
        return point;
    }

    public void setPoint(LatLng point) {
        this.point = point;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public PointOfInterest(LatLng point, float radius, Date endDate, String name, int ID) {
        this.point = point;
        this.radius = radius;
        this.endDate = endDate;
        this.name = name;
        this.ID = ID;
    }

    public static PointOfInterest createRandom() {
        Random rng = new Random();

        LatLng ll = new LatLng(-2.14 - rng.nextDouble() * 0.06, -79.94 + rng.nextDouble() * 0.06);
        PointOfInterest poi = new PointOfInterest(ll, 500 + rng.nextInt(100), null, "null", rng.nextInt());

        return poi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PointOfInterest that = (PointOfInterest) o;
        return Float.compare(that.radius, radius) == 0 &&
                Objects.equals(point, that.point) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(point, radius, endDate);
    }

    @Override
    public String toString() {
        return "PointOfInterest{" +
                "ID=" + ID +
                ", point=" + point +
                ", radius=" + radius +
                '}';
    }


}
